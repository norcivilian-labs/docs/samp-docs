# Design

sa:mp gamemode

competes: gta-open

interacts: server players 

constitutes: gamemode plugin for open.mp

includes: 

resembles: 

patterns: 

target audiences: samp oldfags, c++ nerds

stakeholders: fetsorn, chronosxyz

single server mode in c++ written in open multiplayer sdk

## tasks
- package gamemode with nix

## other gamemodes
https://forum.open.mp/forumdisplay.php?fid=33

| 2024 | https://github.com/sampdevi/samp-json-auth                  |

| 2023 | https://github.com/MrDave1999/Capture-The-Flag              |

| 2023 | https://github.com/cesarurbinas/samp-hyaxe-2                |

| 2023 | https://github.com/daniscript18/clan-wars                   |

| 2023 | https://github.com/cesarurbinas/samp-hyaxe                  |

| 2023 | https://github.com/michael-fa/GTA-City-Remake               |

| 2023 | https://github.com/ThiagoGTH/adrp-gm                        |

| 2023 | https://github.com/ZanderOperator/open.RP                   |

| 2022 | https://github.com/NexiusTailer/ByFly-GTA-SA-GangWar        |

| 2022 | https://github.com/fusez/DTB                                |

| 2022 | https://github.com/lunosat/Ruby-Roleplay                    |

| 2022 | https://github.com/WhiceDev/youtube-tutorial                |

| 2022 | https://github.com/lightningpriest/samp-dayz                |

| 2022 | https://github.com/WhiceDev/altis-life-samp                 |

| 2022 | https://github.com/le01q/samp-gamemode-cwtg                 |

| 2022 | https://github.com/PPC-Trucking/V1                          |

| 2021 | https://github.com/PatrickGTR/gta-open                      |

| 2021 | https://github.com/AbyssMorgan/Zombies-Vs-Humans-Apocalypse |

| 2021 | https://github.com/RogueDrifter/RogueDrifterZ-Drift         |

| 2020 | https://github.com/lucian-nk/Romania-Live-Trucking          |

| 2020 | https://github.com/eoussama/EO-Los-Santos-Cops-and-Robbers  |

| 2020 | https://github.com/Southclaws/samp-prophunt                 |

| 2019 | https://github.com/Open-GTO/Open-GTO                        |

| 2019 | [https://github.com/engjellpireva/MySQL-Login-Register](https://web.archive.org/web/20201101175327/https://github.com/engjellpireva/MySQL-Login-Register)       |

| 2019 | https://github.com/AliLogic/Zombieland-SAMP                 |

| 2018 | https://github.com/MacMailler/GWRP-0.3                      |

| 2018 | https://github.com/TradeWars/gamemode                       |

| 2017 | https://github.com/u4bi-dev/SA-MP_Pokemon_GO                |

| 2017 | https://github.com/ins1x/1nsanedeathmatch                   |

## sa binary
- NT x64 +
- NT arm64 +
- linux x64 + (wine)
- linux aarch64 ? 
- darwin ?

- SA-MP requires GTA:SA (DVD game for PC) v1.00 US/EU.
- Version 2.0 of the DVD version can be downgraded to 1.0 using a third-party patch.
- The Steam, RSGames and Direct2Drive versions of GTA:SA are not currently supported and cannot be downgraded.
- The -R revisions contain bug fixes and security updates. Always use the latest revision.

samp.7z inside .exe binary with client and game, runs on NT
https://cloud.last-mile.chickenkiller.com/s/cg2Kxos3goSB6dz

reversed binary source
https://github.com/gta-reversed/gta-reversed-modern

## sa:mp
- NT x64 +
- NT arm64 vm + (ARM64EC)
- linux x64 + (wine)
- linux aarch64 ? 
- darwin ? 

open.mp launcher
https://github.com/openmultiplayer/launcher

decompilation of sa:mp
https://github.com/dashr9230/SA-MP

leaked old source of sa:mp
https://github.com/mishannn/m0d_sa/tree/master

sa:mp multiplayer mod 0.3.7 binary
https://sa-mp.mp/

## gamemode plugin
- NT x64 + (wsl2 + docker)
- NT arm64 vm ?
- linux x64 + (docker)
- linux aarch64 ? 
- darwin ? 

- samp supports x64 but plugins only support x32
- compile repo with cpp code 
- put to components/plugins in open.mp release

building the c++ plugin

```sh
cd docker
docker compose build
make
```

You may need to set up some directories first:

```sh
mkdir build
mkdir conan
sudo chown 1000 build
sudo chown 1000 conan
```

The output is in docker/build/

VSCode Dev Container feature

This repository supports Dev Container feature. Use the Dev Containers: Reopen in Container command from the Command Palette (F1, Ctrl+Shift+P).

IMPORTANT! At the very first, you should build base Docker container. Go to docker folder and type docker compose build!

To build gamemode inside Dev Container environment, use make in the workspace root folder. Further use SKIP_CMAKE environment variable to prevent repeated CMake configuration process.

## open.mp server
- NT x64 + (wsl2 + docker)
- NT arm64 vm ?
- linux x64 + (docker)
- linux aarch64 ? 
- darwin ? 

- https://github.com/openmultiplayer/open.mp/releases/tag/v1.1.0.2612

Most SAMP plugins are built against the x86 arch, so I run the x86 version of open.mp server, so you should cross-compile the project to x86. 

You should have sampctl installed.
https://github.com/Southclaws/sampctl

Build the server strictly after building C++ part.

```sh
cd server
sampctl ensure
sampctl build
```

Run the server!

```sh
sampctl run
```

If you are in VSCode Dev Container environment, use make run in the workspace root folder to run the server.

## development on m1 
### parallels nt for arm
current choice for sa client

supports x86 binary translation
 - can run samp
 - can't run server
 - can't can compile c++.

Here's the translator, it can run any x86 binary on arm64 nt
https://duckduckgo.com/?q=ARM64EC&t=iphone&ia=web

docker, kvm, wsl2 won't work because m1 parallels doesn't support nested virtualization

### darwin-arm64
unix which has the x86 binary translator. So i could try to run samp in wine there, and compile c++ using the translator.
 - unlikely can run sa + samp in wine

 - maybe can compile x86 c++

Here's the translator, it runs any x86 binary and translates to arm64 instructions at runtime 
https://en.m.wikipedia.org/wiki/Rosetta_(software)

### asahi linux + rosetta
linux-aarch64. has a binary translator. 
 - test if can run sa+sa:mp in wine with xwayland
 - test if can compile x86 c++ with rosetta
 - test if can cross-compile c++ to x86
 - test if dynamic linker breaks c++ plugin with translator

Here's the translator hacked to run on linux 
https://github.com/CathyKMeow/rosetta-linux-asahi

wine for arm64
https://github.com/AndreRH/hangover

### small linux-x86
current choice for samp server and gamemode

 - can run sa + samp in wine
 - see if there's enough
 - maybe can run server (enough memory for server?) 
 - can compile c++ (enough memory for docker?) 

